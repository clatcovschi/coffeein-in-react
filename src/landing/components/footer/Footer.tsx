import * as React from "react";
import { Component } from "react";
import { ThemeProvider } from "@emotion/react";
import { createTheme, Box } from "@mui/material";
import { Link } from "react-router-dom";
import MapLeaflet from "../map/MapLeaflet";
const theme = createTheme({
  palette: {
    text: {
      primary: "#E5E5E5",
      secondary: "#c4c2c2",
    },
  },
});
interface FooterProps {
  logo: string;
  phone: string;
  mail: string;
  instagram: string;
  youtube: string;
  facebook: string;
  coordinates: string;
}

interface FooterState {}

class Footer extends React.Component<FooterProps, FooterState> {
  constructor(props: FooterProps) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <React.Fragment>
        <ThemeProvider theme={theme}>
          <Box
            sx={{
              m: "50px",
              py: "30px",
              color: "#e5e5e5",
              bgcolor: "#1a1a1a",
            }}
          >
            <Box
              sx={{
                display: "flex",
                maxWidth: "95%",
                mx: "auto",
                px: "15px",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Box sx={{ margin: "auto" }}>
                <Link to="/">
                  <Box
                    sx={{
                      mb: "50px",
                    }}
                    component="img"
                    src={this.props.logo}
                  ></Box>
                </Link>

                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      borderRight: "1px  solid  rgba(255, 255, 255, 0.5)",
                      pr: "60px",
                      fontSize: "18px",
                    }}
                  >
                    <Box
                      component="a"
                      href={"tel:" + this.props.phone}
                      sx={{
                        display: "flex",
                        pb: "12px",
                        alignItems: "center",
                      }}
                    >
                      <Box
                        component="img"
                        src="/images/filled-phone.png"
                        sx={{
                          pr: "12px",
                        }}
                      ></Box>
                      <Box component="span">{this.props.phone}</Box>
                    </Box>
                    <Box
                      component="a"
                      href={"mailto:" + this.props.mail}
                      sx={{
                        display: "flex",
                        pb: "12px",
                        alignItems: "center",
                      }}
                    >
                      <Box
                        component="img"
                        src="/images/mail.png"
                        sx={{
                          pr: "12px",
                        }}
                      ></Box>
                      <Box component="span">{this.props.mail}</Box>
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      pl: "60px",
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Box component="a" href={this.props.instagram}>
                      <Box
                        component="img"
                        src="/images/footer-instagram.png"
                        sx={{
                          mx: "8px",
                        }}
                      />
                    </Box>
                    <Box component="a" href={this.props.youtube}>
                      <Box
                        component="img"
                        src="/images/footer-youtube.png"
                        sx={{
                          mx: "8px",
                        }}
                      />
                    </Box>
                    <Box component="a" href={this.props.facebook}>
                      <Box
                        component="img"
                        src="/images/footer-facebook.png"
                        sx={{
                          mx: "8px",
                        }}
                      />
                    </Box>
                  </Box>
                </Box>
              </Box>

              <Box sx={{ m: "auto"}}>
                <MapLeaflet coordinates={this.props.coordinates} />
              </Box>
            </Box>
            <Box
              sx={{
                fontSize: "20px",
                mb: "1rem",
                pt: "30px",
                textAlign: "center",
              }}
            >
              Developed with love by <b>LOOPIT</b>
            </Box>
          </Box>
        </ThemeProvider>
      </React.Fragment>
    );
  }
}

export default Footer;
