import React from 'react';

import {Routes, Route} from 'react-router-dom';

import Home from '../pages/home/Home';
import Products from '../pages/products/Products';
import Cart from '../pages/cart/Cart';


const Rout = () => {
    return (
    <Routes>
      <Route path="/" element={ <Home/> } exact />    
      <Route path="/products/:id" element={ <Products/> } exact />    
      <Route path="/cart/:id" element={ <Cart/> } exact />    
    </Routes>
    );
  };

export default Rout;