import * as React from "react";
import { getCookie } from "../../../../../cookieAccess";
import { createTheme } from "@mui/material/styles";
import { ThemeProvider, Box } from "@mui/material";
import { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


const theme = createTheme({
  palette: {
    text: {
      primary: "#E5E5E5",
      secondary: "#c4c2c2",
    },
  },
});
interface SlickProps {}

interface SlickState {
  items: [
    {
      id: string;
      image: {
        path: string;
      };
      title: [
        {
          shortName: string;
          content: string;
        }
      ];
      description: [
        {
          shortName: string;
          content: string;
        }
      ];
      promoProductInfo: [
        {
          shortName: string;
          content: string;
        }
      ];
    }
  ];
  DataisLoaded: boolean;
}

class Slick extends React.Component<SlickProps, SlickState> {
  url = "http://localhost:3000/api/";

  constructor(props: SlickProps) {
    super(props);
    this.state = {
      items: [
        {
          id: "",
          image: {
            path: "",
          },
          title: [
            {
              shortName: "",
              content: "",
            },
          ],
          description: [
            {
              shortName: "",
              content: "",
            },
          ],
          promoProductInfo: [
            {
              shortName: "",
              content: ``,
            },
          ],
        },
      ],
      DataisLoaded: false,
    };
  }

  componentDidMount() {
    fetch(`${this.url}slider-info`)
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          items: json.docs,
          DataisLoaded: true,
        });
      });
  }
  render() {
    const settings = {
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      arrows: true,
      dots: true,
      infinite: true,
      autoplaySpeed: 1500,
      speed: 1000,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            arrows: false,
          },
        },
      ],
    };
    let i = 0;
    let languages = this.state.items[0].title.map((el) =>
      el.shortName.toUpperCase()
    );
    let lang = getCookie("lang") ? getCookie("lang").toUpperCase() : "RO";
    for (let j = 0; j < languages.length; j++) {
      if (lang === languages[j]) {
        i = j;
      }
    }
    if (!this.state.DataisLoaded) return;
    return (
      <React.Fragment>
        <ThemeProvider theme={theme}>
          <Box
            sx={{
              bgcolor: "#1a1a1a",
              mt: "50px",
              py: "50px",
              px: "100px",
              borderRadius: "30px",
              filter: " drop-shadow(0 30px 10px #000000)",
              backgroundImage: "url('/images/bg2.png')",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "63%",
              backgroundSize: "455px 100%",
              "& .slick-prev:before, .slick-next:before": {
                background: "transparent",
                borderRadius: "50%",
                border: "2px solid",
                color: "#fff",
                fontFamily: "cursive",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                fontSize: "32px",
                lineHeight: "50px",
                width: "30px",
                height: "25px",
                pb: "5px",
              },
              "& .slick-prev:before": {
                content: '"<"',
              },
              "& .slick-next:before": {
                content: '">"',
              },
              "& .slick-prev": {
                ml: "-35px",
              },
              "& .slick-next": {
                mr: "-35px",
              },
              "& .slick-dots": {
                textAlign: "left",
                "& .slick-active": {
                  background: "#2a6028",
                  border: "1px solid #c3c2c2",
                },
                "& li": {
                  width: "12px",
                  height: "12px",
                  background: "#c3c2c2",
                  borderRadius: "50%",
                  mx: "3px",
                },
                "& button:before": {
                  content: "none",
                },
              },
            }}
          >
            <Slider {...settings}>
              {this.state.items.map((el) => (
                <Box
                  key={el.id}
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Box
                    sx={{
                      maxWidth: "44%",
                      mx: "auto",
                    }}
                  >
                    <Box
                      sx={{
                        fontSize: "36px",
                        color: "#c4c2c2",
                        mb: "30px",
                      }}
                    >
                      {el.title[i].content}
                    </Box>
                    <Box>{el.description[i].content}</Box>
                  </Box>
                  <Box
                    sx={{
                      maxWidth: "44%",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      color: "#c4c2c2",

                      "& h4": {
                        height: "32px",
                      },
                    }}
                  >
                    <Box
                      component="img"
                      src={this.url + el.image.path}
                      sx={{
                        width: "18vw",
                        p:"15px"
                      }}
                    />
                    <Box
                      dangerouslySetInnerHTML={{
                        __html: el.promoProductInfo[i].content,
                      }}
                    />
                  </Box>
                </Box>
              ))}
            </Slider>
          </Box>
        </ThemeProvider>
      </React.Fragment>
    );
  }
}

export default Slick;
