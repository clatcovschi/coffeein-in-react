import * as React from 'react';
import { Component } from 'react';

interface LandingProps {
    
}
 
interface LandingState {
    
}
 
class Landing extends React.Component<LandingProps, LandingState> {
    constructor(props: LandingProps) {
        super(props);
        this.state = {   };
    }
    render() { 
        return ( <p>Landing here!</p> );
    }
}
 
export default Landing;